package com.java.json;

import java.io.IOException;

public class JsonSample {
    public static void main(String[] args) throws IOException {
        //simpleJsonEncodingSample();
        simpleJsonDecodingSample();
    }

    static void simpleJsonEncodingSample() throws IOException {
        SimpleJsonExample example = new SimpleJsonExample();
        example.encodeJsonExample();
    }

    static void simpleJsonDecodingSample() {
        SimpleJsonExample example2 = new SimpleJsonExample();
        example2.decodeJsonExample();
    }
}
