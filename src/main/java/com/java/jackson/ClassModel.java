package com.java.jackson;

import com.fasterxml.jackson.dataformat.xml.annotation.JacksonXmlElementWrapper;
import com.fasterxml.jackson.dataformat.xml.annotation.JacksonXmlProperty;

import java.util.ArrayList;
import java.util.List;

public class ClassModel {

    private List<Student> student = new ArrayList<>();

    @JacksonXmlProperty(localName = "student")
    @JacksonXmlElementWrapper(useWrapping = false)
    public List<Student> getStudents() {
        return student;
    }

    public void setStudents(List<Student> students) {
        this.student = students;
    }

    @Override
    public String toString() {
        return "ClassModel{" +
                "students=" + student +
                '}';
    }
}
