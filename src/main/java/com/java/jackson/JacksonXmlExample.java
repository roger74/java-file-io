package com.java.jackson;

import com.fasterxml.jackson.dataformat.xml.XmlMapper;

import java.io.File;
import java.io.IOException;
import java.util.Objects;


public class JacksonXmlExample {

    void writeXMLData() throws IOException {
        XmlMapper mapper = new XmlMapper();
        mapper.writeValue(new File("house-xml.xml"),new HouseModel(25,13,"Beige"));
    }

    /*
    * The Student for this example is based on data from the students.xml
    * */
    void readXMLData() throws IOException {
        XmlMapper mapper = new XmlMapper();
        String student = "<student id=\"753951\">\n" +
                "        <firstname>John</firstname>\n" +
                "        <lastname>Smith</lastname>\n" +
                "        <subject>Science</subject>\n" +
                "        <grade>A</grade>\n" +
                "    </student>";
        StudentModel studentList = mapper.readValue(student, StudentModel.class);
        System.out.println(studentList.toString());
    }

    void readXMLFromFile() throws IOException {
        File students = new File(Objects.requireNonNull(getClass().getClassLoader()
                .getResource("students.xml")).getFile());
        XmlMapper mapper = new XmlMapper();
        ClassModel model = mapper.readValue(students, ClassModel.class);
        System.out.println(model);
    }
}
