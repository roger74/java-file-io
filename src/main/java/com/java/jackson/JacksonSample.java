package com.java.jackson;


import com.fasterxml.jackson.core.JsonProcessingException;

import java.io.IOException;

public class JacksonSample {
    public static void main(String[] args) throws IOException {
        //simpleJacksonRead();
        //simpleJacksonWrite();
        //simpleJacksonNode();
        //simpleJacksonArray();
       // xmlJacksonWrite();
        xmlJacksonRead();
    }

    static void simpleJacksonRead() throws IOException {
        SimpleJacksonExample example = new SimpleJacksonExample();
        //example.readSample1();
        example.readSample2();
    }

    static void simpleJacksonWrite() throws IOException {
        SimpleJacksonExample example = new SimpleJacksonExample();
        example.writeSample1();
    }

    static void simpleJacksonNode() throws JsonProcessingException {
        SimpleJacksonExample example = new SimpleJacksonExample();
        example.nodeSample();
    }

    static void simpleJacksonArray() throws IOException {
        SimpleJacksonExample example = new SimpleJacksonExample();
        example.listSample();
    }

    static void xmlJacksonWrite() throws IOException {
        JacksonXmlExample example = new JacksonXmlExample();
         example.writeXMLData();
    }

    static void xmlJacksonRead() throws IOException {
        JacksonXmlExample example = new JacksonXmlExample();
        // example.readXMLData();
        example.readXMLFromFile();
    }
}
