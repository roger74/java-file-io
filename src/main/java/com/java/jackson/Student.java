package com.java.jackson;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.dataformat.xml.annotation.JacksonXmlProperty;
import com.fasterxml.jackson.dataformat.xml.annotation.JacksonXmlRootElement;

import java.util.Objects;

@JacksonXmlRootElement(localName = "student")
public class Student {
    @JacksonXmlProperty(isAttribute = true)
    private String id;

    private String firstname;
    private String lastname;
    private String subject;
    private String grade;


    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    @Override
    public String toString() {
        return "Student{" +
                "id='" + id + '\'' +
                ", fName='" + getFirstname() + '\'' +
                ", lName='" + getLastname() + '\'' +
                ", subject='" + getSubject() + '\'' +
                ", grade=" + getGrade() +
                '}';
    }

    @JacksonXmlProperty(localName = "firstname")
    public String getFirstname() {
        return firstname;
    }

    public void setFirstname(String fName) {
        this.firstname = fName;
    }

    @JacksonXmlProperty(localName = "lastname")
    public String getLastname() {
        return lastname;
    }

    public void setLastname(String lName) {
        this.lastname = lName;
    }

    @JacksonXmlProperty(localName = "subject")
    public String getSubject() {
        return subject;
    }

    public void setSubject(String subject) {
        this.subject = subject;
    }

    @JacksonXmlProperty(localName = "grade")
    public String getGrade() {
        return grade;
    }

    public void setGrade(String grade) {
        this.grade = grade;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Student student = (Student) o;
        return grade == student.grade && Objects.equals(id, student.id) &&
                Objects.equals(firstname, student.firstname) &&
                Objects.equals(lastname, student.lastname) &&
                Objects.equals(subject, student.subject);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, firstname, lastname, subject, grade);
    }
}
