package com.java.jackson;


import com.fasterxml.jackson.dataformat.xml.annotation.JacksonXmlProperty;
import com.fasterxml.jackson.dataformat.xml.annotation.JacksonXmlRootElement;

import java.util.Objects;

@JacksonXmlRootElement(localName = "student")
public class StudentModel {
    @JacksonXmlProperty(isAttribute = true, localName = "id")
    private String id;

    @JacksonXmlProperty(localName = "firstname")
    private String fName;

    @JacksonXmlProperty(localName = "lastname")
    private String lName;

    @JacksonXmlProperty(localName = "subject")
    private String subject;

    @JacksonXmlProperty(localName = "grade")
    private char grade;

    public StudentModel() {}

    public StudentModel(String id, String fName, String lName, String subject, char grade) {
        this.setId(id);
        this.setfName(fName);
        this.setlName(lName);
        this.setSubject(subject);
        this.setGrade(grade);
    }


    @Override
    public String toString() {
        return "StudentModel{" +
                "id='" + getId() + '\'' +
                ", fName='" + getfName() + '\'' +
                ", lName='" + getlName() + '\'' +
                ", subject='" + getSubject() + '\'' +
                ", grade=" + getGrade() +
                '}';
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        StudentModel that = (StudentModel) o;
        return getGrade() == that.getGrade() && Objects.equals(getId(), that.getId()) && Objects.equals(getfName(), that.getfName()) && Objects.equals(getlName(), that.getlName()) && Objects.equals(getSubject(), that.getSubject());
    }

    @Override
    public int hashCode() {
        return Objects.hash(getId(), getfName(), getlName(), getSubject(), getGrade());
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getfName() {
        return fName;
    }

    public void setfName(String fName) {
        this.fName = fName;
    }

    public String getlName() {
        return lName;
    }

    public void setlName(String lName) {
        this.lName = lName;
    }

    public String getSubject() {
        return subject;
    }

    public void setSubject(String subject) {
        this.subject = subject;
    }

    public char getGrade() {
        return grade;
    }

    public void setGrade(char grade) {
        this.grade = grade;
    }
}
