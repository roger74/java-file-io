package com.java.file;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;

public class ByteArrayStreamExample {

    String sample = "java file input";

    void readArray() {
        byte[] sampleArray = sample.getBytes();

        ByteArrayInputStream bis = new ByteArrayInputStream(sampleArray);
        System.out.println("change values to upper case");
        int amount = bis.available();
        for(int i = 0; i < amount; i++) {
            System.out.println(Character.toUpperCase((char)bis.read()));
        }
        bis.reset();
    }

    void writeArray() throws IOException {
        ByteArrayOutputStream baos = new ByteArrayOutputStream(16);
          baos.write(sample.getBytes());

        byte[] theSample = baos.toByteArray();
        System.out.println("Print out the stream ");
        for(byte b : theSample) {
            System.out.print((char)b);
        }
    }
}
