package com.java.file;

import java.io.IOException;
import java.io.InputStreamReader;

public class StandardStreamExample {
    void example() throws IOException {
        InputStreamReader inRead = null;
        try {
            inRead = new InputStreamReader(System.in);
            System.out.println("Enter characters, 'q' to quit.");
            char c;
            do {
                c = (char) inRead.read();
                System.out.print(c);
            } while (c != 'q');
        } finally {
            if (inRead != null) {
                inRead.close();
            }
        }
    }
}
