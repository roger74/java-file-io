package com.java.file;

import java.io.*;

public class CharacterStreamExample {
    void readPoemV2() throws IOException {
        FileReader myIn = null;
        FileWriter myOut = null;

        try {
            File myPoem = new File(getClass().getClassLoader()
                    .getResource("sample.txt").getFile());
            myIn = new FileReader(myPoem);
            myOut = new FileWriter("sample-character.txt");
            int c;
            while ((c = myIn.read()) != -1) {
                myOut.write(c);
            }
        } catch (NullPointerException ne) {
            System.out.println(ne.getMessage());
        } catch (IOException fe) {
            fe.printStackTrace();
        } finally {
            if (myIn != null) {
                myIn.close();
            }
            if (myOut != null) {
                myOut.close();
            }
        }
    }
}
