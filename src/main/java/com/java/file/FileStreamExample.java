package com.java.file;

import java.io.*;

public class FileStreamExample {
    void readWriteFile() {
        try {
            String message = "Hello from the Java Project.";
            byte[] myBytes = message.getBytes();
            OutputStream myOut = new FileOutputStream("test.txt");
            for (byte b : myBytes) {
                myOut.write(b);
            }
            myOut.close();
            InputStream myIn = new FileInputStream("test.txt");
            int amount = myIn.available();

            for(int i = 0; i < amount; i++) {
                System.out.println((char)myIn.read() +"");
            }
            myIn.close();
        } catch(IOException ex) {
            System.out.println(ex.getMessage());
        }
    }
}
