package com.java.file;

import java.io.*;
import java.util.Objects;

public class ByteStreamExample {
    void readPoem() throws IOException {
        FileInputStream myIn = null;
        FileOutputStream myOut = null;

        try {
             File myPoem = new File(Objects.requireNonNull(getClass().getClassLoader()
                     .getResource("sample.txt")).getFile());
             myIn = new FileInputStream(myPoem);
             myOut = new FileOutputStream("sample-byte.txt");
            int c;
            while ((c = myIn.read()) != -1) {
                myOut.write(c);
            }
        } catch (NullPointerException ne) {
            System.out.println(ne.getMessage());
        } catch (IOException fe) {
            fe.printStackTrace();
        } finally {
            if (myIn != null) {
                myIn.close();
            }
            if (myOut != null) {
                myOut.close();
            }
        }
    }
}
