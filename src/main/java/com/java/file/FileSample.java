package com.java.file;

import java.io.IOException;

public class FileSample {
    public static void main(String[] args) throws IOException {
         byteStreamSample();
        // characterStreamSample();
        //standardStreamSample();
      //  fileStreamSample();
      //  byteArrayStreamSample();
    }

    static void byteStreamSample() throws IOException {
        ByteStreamExample bse = new ByteStreamExample();
        bse.readPoem();
    }

    static void characterStreamSample() throws IOException {
        CharacterStreamExample cse = new CharacterStreamExample();
        cse.readPoemV2();
    }

    static void standardStreamSample() throws IOException {
        StandardStreamExample sse = new StandardStreamExample();
        sse.example();
    }

    static void fileStreamSample() {
        FileStreamExample fse = new FileStreamExample();
        fse.readWriteFile();
    }

    static void byteArrayStreamSample() throws IOException {
        ByteArrayStreamExample bse = new ByteArrayStreamExample();
        //bse.readArray();
        bse.writeArray();
    }
}
