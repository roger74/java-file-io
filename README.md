# Java File Input/Output
This sample project will focus on files and how to read and write to them.

## Description
This sample project will work with .txt, .xml parsing & .json parsing.

## Usage
This project is for reference only and should be utilized in that matter. This project was created
using Maven instead of a basic Java application, so you will need to create a Maven project to follow
along. If there is any Maven related content used, those instructions will be below.


## Project status
Ongoing project that will have updates over time and be updated when appropriate.
